// type dictionery
//nc = new client;
// v1
//
const http = require('http').createServer(),
    io = require('socket.io').listen(http);
http.listen(3000);

const clients = {};
let soccerEvents = [];
const botActions = require('./config/botsActions.js')();

const channels = {
    admins: {totalClients: 0, path: '/admins', channel: io.of('/admins')},
    bots: {totalClients: 0, path: '/bots', channel: io.of('/bots')},
    errors: {totalClients: 0, path: '/errors', channel: io.of('/errors')},
    controllers: {totalClients: 0, path: '/controllers', channel: io.of('/controllers')},
    fuzzers: {totalClients: 0, path: '/fuzzers', channel: io.of('/fuzzers')},
    debuggers: {totalClients: 0, path: '/debuggers', channel: io.of('/debuggers')},
    managers: {totalClients: 0, path: '/managers', channel: io.of('/managers')},
    totalClients: 0
};

function emit(socket, channel, ev, type, msg, lvl) {
    //channels[channel].channel.emit(ev, {ts:new Date(), tp:type, nm:{n:clients[socket.clientID].config.name, s:socket.id}, data:msg, lvl:lvl})
    channels[channel].channel.emit(ev, {
        ts: new Date(),
        tp: type,
        nm: {n: clients[socket.clientID].config ? clients[socket.clientID].config.name : 'lola', s: socket.id},
        data: msg,
        lvl: lvl
    });
    if (channel === 'debuggers') channels['admins'].channel.emit('debug', {
        ts: new Date(),
        tp: type,
        nm: {n: clients[socket.clientID].config ? clients[socket.clientID].config.name : 'lola', s: socket.id},
        data: msg,
        lvl: lvl
    })
}

function authenticate(socket, data, callback) {
    const db = require('../fakedb.json');
    let username = data.username;
    let password = data.password;
    let pswd = db[username];
    console.log('authenticating client : ', data, ' with ', db);
    return callback(null, true);
    if (pswd === password) {
        console.log('i`m in');
        return callback(null, true);
    } else {
        console.log('i`m error out');
        emit(socket, 'errors', 'error-handling', 'error', ('unauthenticated with error:' + error), 0);
        return callback(new Error("User not found"));
    }
}

function postAuthenticate(socket, data) {

}

/**
 * Initializind Clients with socket.id and type and
 * push them into clients dictionary. Also put the default listeners on socket.
 * On every channel connection we must call this function.
 *
 * @param socket is the client socket
 * @param type is the type of the client (bot, admin, fuzzer, ...)
 */
function initClientConnection(socket, type) {
    socket.clientID = socket.id;
    socket.type = type;
    clients[socket.clientID] = {
        socket: socket,
        socketID: socket.id,
        name: '',
        type: type,
        actions: {},
        lastError: '',
        status: '',
        username: '',
        password: ''
    };
    if (type === 'bots') {
        clients[socket.clientID].eventsToParse = {};
    }
    channels[type].totalClients++;
    channels.totalClients++;

    socket.on('disconnect', function () {
        closeClientConnection(socket)
    });

    socket.on('error-handling', function (data) {
        let retObj = Object.assign({}, clients[socket.clientID]);
        delete retObj.socket;
        emit(socket, 'errors', 'error-handling', 'error', data, 0);
        //console.log(data);
    });

    let obj = Object.assign({}, clients[socket.id]);
    delete obj.socket;
    if (type !== 'bots')
        emit(socket, 'admins', 'client-connected', 'nc', obj, 0);

    // socket.on('authentication', function(data) {
    //     authenticate(socket, data, function(err, success) {
    //         if (success) {
    //             socket.emit('authenticated', success);
    //             return postAuthenticate(socket, data);
    //         } else if (err) {
    //             socket.emit('unauthorized', {message: err.message}, function() {
    //                 socket.disconnect();
    //             });
    //         } else {
    //             socket.emit('unauthorized', {message: 'Authentication failure'}, function() {
    //                 socket.disconnect();
    //             });
    //         }
    //     });
    // });
}

/**
 * Closing the connection and delete client from the clients dictionary
 *
 * @param socket is the client socket :P
 */
function closeClientConnection(socket) {
    channels[socket.type].totalClients--;
    channels.totalClients--;
    let ret = Object.assign({}, clients[socket.id]);
    delete ret.socket;
    //channels['errors'].channel.emit('error', {client:ret, data:'Vgike kapoios !!!'});
    let obj = Object.assign({}, clients[socket.id]);
    delete obj.socket;
    emit(socket, 'admins', 'client-disconnected', 'cd', obj, 0);

    //channels['admins'].channel.emit('client-disconnected', ret);
    delete clients[socket.id];
    console.log('client disconnected');
    io.emit('message', 'client disconnected ID : ', socket.id);
}

/**
 * Managers channel administration
 */
channels['managers'].channel.on('connection', function (socket) {
    initClientConnection(socket, 'managers');
    let ret = Object.assign({}, clients[socket.id]);
    delete ret.socket;
    //channels['admins'].channel.emit('client-connected', ret);
    //channels['admins'].channel.emit('client-connected', clients[socket.id]);
    socket.on('give-jobs', function (data) {
        socket.emit('jobs', botActions.soccer.eventManager);
    });
    socket.on('executing', function (data) {
        //console.log(data);
    });
    socket.on('error', function (data) {
        //console.log(data);
        emit(socket, 'admins', 'error-from-amagers', 'me', data, 0);
        //channels['admins'].channel.emit('error-from-amagers', data);
    });
    socket.on('send-data', function (data) {
        //console.log('Manager send data ok.');
        eventsHandler(data);
        soccerEvents = data;
        if (watchEvents.added.inPlay.length > 0) distributeInPlayEvents(watchEvents.added.inPlay, 'bet365inPlay', 'inPlay');
        if (watchEvents.added.preGame.length > 0) distributeInPlayEvents(watchEvents.added.preGame, 'bet365preGame', 'preGame');
        if (watchEvents.deleted.inPlay.length > 0) deleteInPlayEvents(watchEvents.deleted.inPlay, 'bet365inPlay', 'inPlay');
        if (watchEvents.deleted.preGame.length > 0) deleteInPlayEvents(watchEvents.deleted.preGame, 'bet365preGame', 'preGame');
        watchEvents.changes = soccerEvents.changes;
        emit(socket, 'admins', 'set-events', 'se', watchEvents, 0);
        //channels['admins'].channel.emit('set-events', watchEvents);
    });
});

let watchEvents = {
    parsing: [],
    added: {inPlay: [], preGame: []}, deleted: {inPlay: [], preGame: []},
    changes: {added: {inPlay: [], preGame: []}, deleted: {inPlay: [], preGame: []}}
};
let botsEvents = {};

function deleteInPlayEvents(events) {
    for (let ev of events) {
        clients[botsEvents[ev.h + ' v ' + ev.g]].socket.emit('del-event', ev);
        let obj = Object.assign({}, clients[keyclient]);
        delete obj.socket;
        emit(clients[botsEvents[ev.h + ' v ' + ev.g]].socket, 'admins', 'delete-bot-event', 'de', {
            bot: obj,
            ev: ev
        }, 0);

        delete clients[keyclient].eventsToParse[ev.h + ' v ' + ev.g];
        delete ev;
    }
}

function distributeInPlayEvents(events, title, type) {
    //console.log('distributing event ', title, events.length);
    //for (let ev of events) {
    for (let keyclient of Object.keys(clients)) {
        if (clients[keyclient].config)
        //('client ', clients[keyclient].config);
            if (clients[keyclient].config && clients[keyclient].config.type === title &&
                clients[keyclient].config.availablePages > 0 && clients[keyclient].status === 'ready') {
                let index = Math.floor(Math.random() * events.length);
                let ev = events[index];
                console.log('giving event to bot ', ev);
                clients[keyclient].config.availablePages--;
                clients[keyclient].socket.emit('set-event', ev);
                clients[keyclient].eventsToParse[ev.h + ' v ' + ev.g] = ev;
                let obj = Object.assign({}, clients[keyclient]);
                delete obj.socket;
                emit(clients[keyclient].socket, 'admins', 'set-bot-event', 'sbe', {bot: obj, ev: ev}, 0);
                let evTmp = events.splice(index, 1)[0];
                evTmp.type = type;
                watchEvents.parsing.push(evTmp);
            }
    }
    //}
}

function eventsHandler(data) {
    let insertedInPlayGames = data.changes.added.inPlay;
    let insertedPreGames = data.changes.added.preGame;

    if (Object.keys(insertedInPlayGames).length > 0)
        for (let key of Object.keys(insertedInPlayGames)) {
            let ret = {h: insertedInPlayGames[key].h, g: insertedInPlayGames[key].g, page: []};
            watchEvents.added.inPlay.push(ret);
        }

    if (Object.keys(insertedPreGames).length > 0)
        for (let key of Object.keys(insertedPreGames)) {
            let ret = {h: insertedPreGames[key].h, g: insertedPreGames[key].g, page: []};
            watchEvents.added.preGame.push(ret);
        }
}

/**
 * Bots channel administration
 */
channels['bots'].channel.on('connection', function (socket) {
    initClientConnection(socket, 'bots');
    socket.emit('startingAt', clients.totalClients);

    socket.on('error', function (data) {
        channels.errors.channel.emit('error', data);
        //console.log('bot-error',data);
    });
    socket.on('to-admins', function (data) {
        emit(socket, 'admins', 'from bot', 'mfb', data, 0);
    });

    channels['admins'].channel.emit('message', 'new bot connected ' + socket.id);

    socket.on('bot-exec', function (data) {
        channels['controllers'].channel.emit('bot-exec', {bot: socket.id, d: data});
        emit(socket, 'admins', 'bot-exec', 'bex', {bot: socket.id, d: data}, 0);
    });

    socket.on('message', function (data) {
    });

    socket.on('debug', function (data) {
        emit(socket, 'debuggers', 'message', 'dbg', data, 0);
        console.log('debug -> ', data);
    });

    socket.on('get-jobs', function (data) {
        socket.emit('jobs', botActions.soccer[data]);
        clients[socket.id].status = 'ready';
    });

    socket.on('send-data', function (data) {
        channels['fuzzers'].channel.emit('bot-data', {bot: socket.id, d: data});
        console.log('Client-Bot send data ok.');
    });

    socket.on('set-identity', function (data) {
        console.log('set-identity', data);
        clients[socket.id].config = data;
        let obj = Object.assign({}, clients[socket.id]);
        delete obj.socket;
        emit(socket, 'admins', 'client-connected', 'cc', obj, 0);
    });
});

/**
 * Admins channel administration
 */
channels['admins'].channel.on('connection', function (socket) {
    initClientConnection(socket, 'admins');
    socket.on('get-events', function (data) {
        emit(socket, 'admins', 'set-events', 'sme', watchEvents, 0);
    });
    socket.on('delete-bot-event', (data) => {
        console.log('delete event from bot ', data);
        clients[data.s].socket.emit('del-event', data.ev);
        let tmpEvent = watchEvents.parsing.find(ff => (ff.h === data.ev.h && ff.g === data.ev.g));
        watchEvents.added[tmpEvent.type].push(tmpEvent);
    });
    emit(socket, 'admins', 'set-events', 'sme', watchEvents, 0);

    let ret = [];
    for (let cl of Object.keys(clients)) {
        let obj = Object.assign({}, clients[cl]);
        delete obj.socket;
        ret.push(obj);
    }
    socket.emit('init', ret);
    console.log('admin connected');
    socket.emit('message', 'You are connected to admins channel!');
    socket.on('get-channels', function (fn) {
        let retChannels = [];
        for (let chnl of Object.keys(channels)) {
            let retObj = Object.assign({}, channels[chnl]);
            delete retObj.channel;
            retChannels.push(retObj);
        }
        //channels['admins'].channel.emit('client-connected', retChannels);
        fn(retChannels);
    });

    socket.on('set-bot-mode', function (data) {
        clients[data.id].socket.emit('set-mode', data.md);
    });
    socket.on('set-bot-event',function (data) {
        clients[data.id].socket.emit('set-event',data.e);
    });
});

channels['errors'].channel.on('connection', function (socket) {
    initClientConnection(socket, 'errors');
    let ret = Object.assign({}, clients[socket.id]);
    delete ret.socket;
    //channels['admins'].channel.emit('client-connected', ret);
});

io.sockets.on('connection', function (socket) {
    console.log('A client is connected!');
    /* socket.on('join-to-channel', (data) => {
         console.log('redirect request client to channel!', data);
         socket.join(data);
     });*/
});

/*require('socketio-auth')(io, {
    authentication: authenticate,
    postAuthenticate: postAuthenticate,
    //disconnect: closeClientConnection,
    timeout: 10000
});*/

require('./socket-auth.js')(io, {
    authentication: authenticate,
    postAuthenticate: postAuthenticate,
    //disconnect: closeClientConnection,
    timeout: 10000
});

console.log('Server up @ port 3000');