const b365ipFormat = {
    sport:'football',
    type:'inPlay',
    ev:'paok v aris',
    markets : [
        {name:'Fulltime Result', odds:[{h:2.50, draw:3.20, g:1.5}]},
        {name:'Double Chance', odds:[{h:2.50, draw:3.20, g:1.5}]},
        {name:'Half Time Result', odds:[{h:2.50, draw:3.20, g:1.5}]},
        {name:'1st_Goal', odds:[{h:2.50, no:3.20, g:1.5}]},
        {name:'Match Goals', odds:[{noname:2.50, over:3.20, under:1.5}]},
        {name:'Alternative Match Goals', odds:[{noname:2.50, over:3.20, under:1.5}]},
        {name:'First Half Goals', odds:[{noname:2.50, over:3.20, under:1.5}]},
        {name:'Match Corners', odds:[{noname:2.50, over:3.20, exactly:0.3, under:1.5}]},
        {name:'2-Way Corners', odds:{over:{o1:0.5,o2:3.20}, under:{o1:0.5,o2:3.20}}},
        {name:'1st Half Corners', odds:[{noname:2.50, over:3.20, exactly:0.3, under:1.5}]},
        {name:'Asian Handicap (0-0)', h:{odds:[{o1:2.50, o2:3.20}], g:{odds:[{o1:0.3, o2:1.5}]}}},
        {name:'Goal Line (0-0)', odds:[{noname:2.50, over:3.20, under:1.5}]},
        {name:'1st Half Asian Handicap (0-0)', h:{odds:[{o1:'0.0,+2.500', o2:3.20}], g:{odds:[{o1:'0.0,+2.500', o2:1.5}]}}},
        {name:'1st Half Goal Line (0-0)', odds:[{noname:2.50, over:3.20, under:1.5}]},
        {name:'Half Time/Full Time', h:{odds:[{o1:'lektiko', o2:3.20}], g:{odds:[{o1:'lektiko', o2:1.5}]}}},
        {name:'Half Time Correct Score', '1':[{o1:'lektiko', o2:3.20}], 'X':[{o1:'lektiko', o2:1.5}], '2':[{o1:'lektiko', o2:1.5}]},
        {name:'Final Score', '1':[{o1:'lektiko', o2:3.20}], 'X':[{o1:'lektiko', o2:1.5}], '2':[{o1:'lektiko', o2:1.5}]},
        {name:'Asian Corners', odds:[{noname:2.50, over:3.20, under:1.5}]},
        {name:'1st Half Asian Corners', odds:[{noname:2.50, over:3.20, under:1.5}]},
        {name:'Corners Race', odds:[{noname:'lektiko', '7':3.20, '9':1.5}]},
        {name:'Corner Handicap', odds:[{noname:'lektiko', o1:3.20}]},
        {name:'Most Corners', odds:[{noname:'lektiko', o1:3.20}]},
        {name:'Corners', odds:[{noname:'lektiko', 'lektiko':3.20, 'last':0.4}]},
        {name:'Number Of Cards', odds:[{noname:'lektiko', o1:3.20}]},
        {name:'3-Way Handicap', odds:{h:[{'lektiko':'+2', o1:3.20}], draw:[{'lektiko':'+2', o1:3.20}],g:[{'lektiko':'+2', o1:3.20}]}},
        {name:'1st Half - Handicap', odds:{h:[{'lektiko':'+2', o1:3.20}], draw:[{'lektiko':'+2', o1:3.20}],g:[{'lektiko':'+2', o1:3.20}]}},
        {name:'Draw No Bet', odds:[{noname:'lektiko', o1:3.20}]},
        {name:'Last Team To Score', odds:[{noname:'lektiko', o1:3.20}]},
        {name:'Goals Odd/Even', odds:[{odd:'lektiko', o1:3.20}]},
        {name:'Result / Both Teams To Score', odds:[{noname:'lektiko', '7':3.20, '9':1.5}]},
        {name:'Both Teams To Score', odds:[{noname:'lektiko', '7':3.20}]},
        {name:'Both Teams To Score in 1st Half', odds:[{noname:'lektiko', '7':3.20}]},
        {name:'Both Teams To Score in 2nd Half', odds:[{noname:'lektiko', '7':3.20}]},
        {name:'Teams Clean Sheet', odds:{h:{'yes':0.4, no:3.20}, g:{'yes':0.4, no:3.20}}},
        {name:'$HOME Exact Goals', odds:[{noname:'lektiko', '7':3.20}]},
        {name:'$GUEST Exact Goals', odds:[{noname:'lektiko', '7':3.20}]},
        {name:'$HOME Exact Goals', odds:[{noname:2.50, over:3.20, under:1.5}]},
        {name:'$GUEST Exact Goals', odds:[{noname:2.50, over:3.20, under:1.5}]},
    ]
}
module.exports = function () {
}
{ "sport":
  Double Chance↵Naestved IF or Draw↵1.36↵Nykobing or Draw↵1.50↵Naestved IF or Nykobing↵1.33↵Half Time Result↵Naestved IF↵3.25↵Draw↵2.20↵Nykobing↵3.25↵1st Goal↵Naestved IF↵1.95↵No 1st Goal↵11.00↵Nykobing↵1.95↵Match Goals↵ ↵2.5↵Over↵1.83↵Under↵1.83↵Alternative Match Goals↵ ↵0.5↵1.5↵3.5↵4.5↵5.5↵6.5↵Over↵1.050↵1.25↵3.25↵6.00↵13.00↵26.00↵Under↵11.00↵3.75↵1.33↵1.12↵1.040↵1.010↵First Half Goals↵ ↵0.5↵1.5↵2.5↵3.5↵Over↵1.40↵2.75↵7.00↵21.00↵Under↵2.75↵1.40↵1.10↵1.015↵Match Corners↵Current Corners: 0↵ ↵8↵9↵10↵11↵12↵Over↵1.44↵1.72↵2.20↵2.75↵3.75↵Exactly↵8.00↵7.50↵8.50↵8.50↵9.50↵Under↵3.75↵2.62↵2.00↵1.61↵1.40↵2-Way Corners↵Over↵9.5↵1.72↵Under↵9.5↵2.00↵1st Half Corners↵Over↵5↵2.60↵Exactly↵5↵5.50↵Under↵5↵1.95↵Asian Handicap (0-0)↵Naestved IF↵+0.5↵1.475↵0.0,+0.5↵1.625↵0.0↵1.925↵0.0,-0.5↵2.250↵-0.5↵2.600↵Nykobing↵-0.5↵2.600↵0.0,-0.5↵2.250↵0.0↵1.925↵0.0,+0.5↵1.625↵+0.5↵1.475↵Goal Line (0-0)↵ ↵2↵2.0,2.5↵2.5↵2.5,3.0↵3↵Over↵1.400↵1.650↵1.900↵2.075↵2.425↵Under↵2.850↵2.200↵1.950↵1.725↵1.525↵1st Half Asian Handicap (0-0)↵Naestved IF↵+0.5↵1.325↵0.0,+0.5↵1.500↵0.0↵1.900↵0.0,-0.5↵2.500↵-0.5↵3.300↵Nykobing↵-0.5↵3.300↵0.0,-0.5↵2.500↵0.0↵1.900↵0.0,+0.5↵1.500↵+0.5↵1.325↵1st Half Goal Line (0-0)↵ ↵0.5↵0.5,1.0↵1↵1.0,1.5↵1.5↵Over↵1.400↵1.550↵1.850↵2.350↵2.750↵Under↵2.850↵2.375↵1.950↵1.575↵1.425↵Half Time/Full Time↵Naestved IF - Naestved IF↵4.33↵Naestved IF - Draw↵13.00↵Naestved IF - Nykobing↵26.00↵Draw - Naestved IF↵6.00↵Draw - Draw↵5.00↵Draw - Nykobing↵6.00↵Nykobing - Naestved IF↵26.00↵Nykobing - Draw↵13.00↵Nykobing - Nykobing↵4.33↵Half Time Correct Score↵SliderAll↵1↵1-0↵4.50↵2-0↵13.00↵2-1↵21.00↵3-0↵41.00↵3-1↵51.00↵X↵0-0↵2.75↵1-1↵7.00↵2-2↵41.00↵2↵1-0↵4.50↵2-0↵13.00↵2-1↵21.00↵3-0↵41.00↵3-1↵51.00↵Final Score↵SliderAll↵1↵1-0↵9.50↵2-0↵13.00↵2-1↵10.00↵3-0↵26.00↵3-1↵19.00↵3-2↵26.00↵4-0↵51.00↵4-1↵41.00↵4-2↵51.00↵4-3↵81.00↵5-0↵101.00↵5-1↵81.00↵5-2↵126.00↵X↵0-0↵11.00↵1-1↵6.00↵2-2↵12.00↵3-3↵41.00↵2↵1-0↵9.50↵2-0↵13.00↵2-1↵10.00↵3-0↵26.00↵3-1↵19.00↵3-2↵26.00↵4-0↵51.00↵4-1↵41.00↵4-2↵51.00↵4-3↵81.00↵5-0↵101.00↵5-1↵81.00↵5-2↵126.00↵Asian Corners↵ ↵10↵Over↵1.975↵Under↵1.825↵1st Half Asian Corners↵ ↵4.5↵Over↵1.800↵Under↵2.000↵Corners Race↵ ↵Naestved IF↵Nykobing↵Neither↵3↵1.80↵1.90↵41.00↵5↵2.10↵2.40↵4.50↵7↵3.50↵4.00↵1.72↵9↵8.00↵10.00↵1.12↵Corners↵ ↵Naestved IF↵Nykobing↵2nd Corner↵1.80↵1.90↵Last↵1.80↵1.90↵
    3-Way Handicap↵Naestved IF↵Draw↵Nykobing↵+3↵1.035↵-3↵13.00↵-3↵19.00↵+2↵1.12↵-2↵7.50↵-2↵11.00↵+1↵1.36↵-1↵4.33↵-1↵5.00↵-1↵5.00↵+1↵4.33↵+1↵1.50↵-2↵11.00↵+2↵7.50↵+2↵1.12↵
    1st Half - Handicap↵Naestved IF↵Draw↵Nykobing↵+1↵1.30↵-1↵4.00↵-1↵10.00↵+2↵1.050↵-2↵11.00↵-2↵23.00↵-1↵10.00↵+1↵4.00↵+1↵1.30↵Draw No Bet↵Naestved IF↵1.83↵Nykobing↵1.83↵Last Team to Score↵Naestved IF↵1.95↵No Goals↵11.00↵Nykobing↵1.95↵Goals Odd/Even↵Odd↵1.90↵Even↵1.80↵Result / Both Teams To Score↵ ↵Naestved IF↵Nykobing↵Draw↵Yes↵5.00↵5.00↵4.33↵No↵5.00↵5.00↵11.00↵Both Teams to Score↵Yes↵1.66↵No↵2.10↵Both Teams to Score in 1st Half↵Yes↵4.33↵No↵1.20↵Both Teams to Score in 2nd Half↵Yes↵3.25↵No↵1.33↵Team Clean Sheet↵Naestved IF↵Yes↵3.50↵No↵1.28↵Nykobing↵Yes↵3.50↵No↵1.28↵Naestved IF Exact Goals↵0 Goals↵3.50↵1 Goal↵2.50↵2 Goals↵3.75↵3+ Goals↵5.50↵Nykobing Exact Goals↵0 Goals↵3.50↵1 Goal↵2.50↵2 Goals↵3.75↵3+ Goals↵5.50↵Naestved IF Goals↵ ↵1.5↵2.5↵3.5↵Over↵2.37↵5.50↵15.00↵Under↵1.53↵1.14↵1.030↵Nykobing Goals↵ ↵1.5↵2.5↵3.5↵Over↵2.37↵5.50↵15.00↵Under↵1.53↵1.14↵1.030↵Team to Score in Both Halves↵Naestved IF↵Yes↵3.75↵No↵1.25↵Nykobing↵Yes↵3.75↵No↵1.25↵To Win 2nd Half↵Naestved IF↵2.75↵Draw↵2.50↵Nykobing↵2.75↵Team to Score in 2nd Half↵Naestved IF↵Yes↵1.72↵No↵2.00↵Nykobing↵Yes↵1.72↵No↵2.00↵Next 10 Minutes (00:00 - 09:59)↵ ↵Over↵Under↵Goals↵0.5↵4.33↵0.5↵1.20↵Corners↵0.5↵1.61↵0.5↵2.20↵Match Time Result↵ ↵Naestved IF↵Draw↵Nykobing↵10:00↵9.50↵1.16↵9.50↵20:00↵4.75↵1.44↵4.75↵30:00↵3.75↵1.66↵3.75↵40:00↵3.40↵1.95↵3.40↵50:00↵3.00↵2.30↵3.00↵60:00↵2.75↵2.50↵2.75↵70:00↵2.75↵2.62↵2.75↵80:00↵2.62↵2.75↵2.62↵Time of 1st Goal↵ ↵Goal Before↵No Goal Before↵10:00↵4.33↵1.20↵20:00↵2.50↵1.50↵30:00↵1.80↵1.90↵40:00↵1.50↵2.50↵50:00↵1.28↵3.50↵60:00↵1.18↵4.50↵70:00↵1.12↵6.00↵80:00↵1.10↵7.00↵Time of 1st Goal - Naestved IF↵ ↵Goal Before↵No Goal Before↵10:00↵8.00↵1.083↵20:00↵4.33↵1.20↵30:00↵3.00↵1.36↵40:00↵2.37↵1.53↵50:00↵1.83↵1.83↵60:00↵1.61↵2.20↵70:00↵1.50↵2.50↵80:00↵1.40↵2.75↵Time of 1st Goal - Nykobing↵ ↵Goal Before↵No Goal Before↵10:00↵8.00↵1.083↵20:00↵4.33↵1.20↵30:00↵3.00↵1.36↵40:00↵2.37↵1.53↵50:00↵1.83↵1.83↵60:00↵1.61↵2.20↵70:00↵1.50↵2.50↵80:00↵1.40↵2.75"
"Fulltime Result↵Naestved IF↵2.50↵Draw↵3.20↵Nykobing↵2.80↵"
    Double Chance↵Naestved IF or Draw↵1.36↵Nykobing or Draw↵1.50↵Naestved IF or Nykobing↵1.33↵
    Half Time Result↵Naestved IF↵3.25↵Draw↵2.20↵Nykobing↵3.25↵
    1st Goal↵Naestved IF↵1.95↵No 1st Goal↵11.00↵Nykobing↵1.95↵
    Match Goals↵ ↵2.5↵Over↵1.83↵Under↵1.83↵
    Alternative Match Goals↵ ↵0.5↵1.5↵3.5↵4.5↵5.5↵6.5↵Over↵1.050↵1.25↵3.25↵6.00↵13.00↵26.00↵Under↵11.00↵3.75↵1.33↵1.12↵1.040↵1.010↵
    First Half Goals↵ ↵0.5↵1.5↵2.5↵3.5↵Over↵1.40↵2.75↵7.00↵21.00↵Under↵2.75↵1.40↵1.10↵1.015↵
    Match Corners↵Current Corners: 0↵ ↵8↵9↵10↵11↵12↵Over↵1.44↵1.72↵2.20↵2.75↵3.75↵Exactly↵8.00↵7.50↵8.50↵8.50↵9.50↵Under↵3.75↵2.62↵2.00↵1.61↵1.40↵
    2-Way Corners↵Over↵9.5↵1.72↵Under↵9.5↵2.00↵
    1st Half Corners↵Over↵5↵2.60↵Exactly↵5↵5.50↵Under↵5↵1.95↵
    Asian Handicap (0-0)↵Naestved IF↵+0.5↵1.475↵0.0,+0.5↵1.625↵0.0↵1.925↵0.0,-0.5↵2.250↵-0.5↵2.600↵Nykobing↵-0.5↵2.600↵0.0,-0.5↵2.250↵0.0↵1.925↵0.0,+0.5↵1.625↵+0.5↵1.475↵
    Goal Line (0-0)↵ ↵2↵2.0,2.5↵2.5↵2.5,3.0↵3↵Over↵1.400↵1.650↵1.900↵2.075↵2.425↵Under↵2.850↵2.200↵1.950↵1.725↵1.525↵
    1st Half Asian Handicap (0-0)↵Naestved IF↵+0.5↵1.325↵0.0,+0.5↵1.500↵0.0↵1.900↵0.0,-0.5↵2.500↵-0.5↵3.300↵Nykobing↵-0.5↵3.300↵0.0,-0.5↵2.500↵0.0↵1.900↵0.0,+0.5↵1.500↵+0.5↵1.325↵
    1st Half Goal Line (0-0)↵ ↵0.5↵0.5,1.0↵1↵1.0,1.5↵1.5↵Over↵1.400↵1.550↵1.850↵2.350↵2.750↵Under↵2.850↵2.375↵1.950↵1.575↵1.425↵

    Half Time/Full Time↵Naestved IF - Naestved IF↵4.33↵Naestved IF - Draw↵13.00↵Naestved IF - Nykobing↵26.00↵Draw - Naestved IF↵6.00↵Draw - Draw↵5.00↵Draw - Nykobing↵6.00↵Nykobing - Naestved IF↵26.00↵Nykobing - Draw↵13.00↵Nykobing - Nykobing↵4.33↵

    Half Time Correct Score↵SliderAll↵1↵1-0↵4.50↵2-0↵13.00↵2-1↵21.00↵3-0↵41.00↵3-1↵51.00↵X↵0-0↵2.75↵1-1↵7.00↵2-2↵41.00↵2↵1-0↵4.50↵2-0↵13.00↵2-1↵21.00↵3-0↵41.00↵3-1↵51.00↵
    Final Score↵SliderAll↵1↵1-0↵9.50↵2-0↵13.00↵2-1↵10.00↵3-0↵26.00↵3-1↵19.00↵3-2↵26.00↵4-0↵51.00↵4-1↵41.00↵4-2↵51.00↵4-3↵81.00↵5-0↵101.00↵5-1↵81.00↵5-2↵126.00↵X↵0-0↵11.00↵1-1↵6.00↵2-2↵12.00↵3-3↵41.00↵2↵1-0↵9.50↵2-0↵13.00↵2-1↵10.00↵3-0↵26.00↵3-1↵19.00↵3-2↵26.00↵4-0↵51.00↵4-1↵41.00↵4-2↵51.00↵4-3↵81.00↵5-0↵101.00↵5-1↵81.00↵5-2↵126.00↵
    Asian Corners↵ ↵10↵Over↵1.975↵Under↵1.825↵1st Half Asian Corners↵ ↵4.5↵Over↵1.800↵Under↵2.000↵Corners Race↵ ↵Naestved IF↵Nykobing↵Neither↵3↵1.80↵1.90↵41.00↵5↵2.10↵2.40↵4.50↵7↵3.50↵4.00↵1.72↵9↵8.00↵10.00↵1.12↵Corners↵ ↵Naestved IF↵Nykobing↵2nd Corner↵1.80↵1.90↵Last↵1.80↵1.90↵3-Way Handicap↵Naestved IF↵Draw↵Nykobing↵+3↵1.035↵-3↵13.00↵-3↵19.00↵+2↵1.12↵-2↵7.50↵-2↵11.00↵+1↵1.36↵-1↵4.33↵-1↵5.00↵-1↵5.00↵+1↵4.33↵+1↵1.50↵-2↵11.00↵+2↵7.50↵+2↵1.12↵1st Half - Handicap↵Naestved IF↵Draw↵Nykobing↵+1↵1.30↵-1↵4.00↵-1↵10.00↵+2↵1.050↵-2↵11.00↵-2↵23.00↵-1↵10.00↵+1↵4.00↵+1↵1.30↵Draw No Bet↵Naestved IF↵1.83↵Nykobing↵1.83↵Last Team to Score↵Naestved IF↵1.95↵No Goals↵11.00↵Nykobing↵1.95↵Goals Odd/Even↵Odd↵1.90↵Even↵1.80↵Result / Both Teams To Score↵ ↵Naestved IF↵Nykobing↵Draw↵Yes↵5.00↵5.00↵4.33↵No↵5.00↵5.00↵11.00↵Both Teams to Score↵Yes↵1.66↵No↵2.10↵Both Teams to Score in 1st Half↵Yes↵4.33↵No↵1.20↵Both Teams to Score in 2nd Half↵Yes↵3.25↵No↵1.33↵Team Clean Sheet↵Naestved IF↵Yes↵3.50↵No↵1.28↵Nykobing↵Yes↵3.50↵No↵1.28↵Naestved IF Exact Goals↵0 Goals↵3.50↵1 Goal↵2.50↵2 Goals↵3.75↵3+ Goals↵5.50↵Nykobing Exact Goals↵0 Goals↵3.50↵1 Goal↵2.50↵2 Goals↵3.75↵3+ Goals↵5.50↵Naestved IF Goals↵ ↵1.5↵2.5↵3.5↵Over↵2.37↵5.50↵15.00↵Under↵1.53↵1.14↵1.030↵Nykobing Goals↵ ↵1.5↵2.5↵3.5↵Over↵2.37↵5.50↵15.00↵Under↵1.53↵1.14↵1.030↵Team to Score in Both Halves↵Naestved IF↵Yes↵3.75↵No↵1.25↵Nykobing↵Yes↵3.75↵No↵1.25↵To Win 2nd Half↵Naestved IF↵2.75↵Draw↵2.50↵Nykobing↵2.75↵Team to Score in 2nd Half↵Naestved IF↵Yes↵1.72↵No↵2.00↵Nykobing↵Yes↵1.72↵No↵2.00↵Next 10 Minutes (00:00 - 09:59)↵ ↵Over↵Under↵Goals↵0.5↵4.33↵0.5↵1.20↵Corners↵0.5↵1.61↵0.5↵2.20↵Match Time Result↵ ↵Naestved IF↵Draw↵Nykobing↵10:00↵9.50↵1.16↵9.50↵20:00↵4.75↵1.44↵4.75↵30:00↵3.75↵1.66↵3.75↵40:00↵3.40↵1.95↵3.40↵50:00↵3.00↵2.30↵3.00↵60:00↵2.75↵2.50↵2.75↵70:00↵2.75↵2.62↵2.75↵80:00↵2.62↵2.75↵2.62↵Time of 1st Goal↵ ↵Goal Before↵No Goal Before↵10:00↵4.33↵1.20↵20:00↵2.50↵1.50↵30:00↵1.80↵1.90↵40:00↵1.50↵2.50↵50:00↵1.28↵3.50↵60:00↵1.18↵4.50↵70:00↵1.12↵6.00↵80:00↵1.10↵7.00↵Time of 1st Goal - Naestved IF↵ ↵Goal Before↵No Goal Before↵10:00↵8.00↵1.083↵20:00↵4.33↵1.20↵30:00↵3.00↵1.36↵40:00↵2.37↵1.53↵50:00↵1.83↵1.83↵60:00↵1.61↵2.20↵70:00↵1.50↵2.50↵80:00↵1.40↵2.75↵Time of 1st Goal - Nykobing↵ ↵Goal Before↵No Goal Before↵10:00↵8.00↵1.083↵20:00↵4.33↵1.20↵30:00↵3.00↵1.36↵40:00↵2.37↵1.53↵50:00↵1.83↵1.83↵60:00↵1.61↵2.20↵70:00↵1.50↵2.50↵80:00↵1.40↵2.75"}

const dictionary = {
    3P: 'PLACE_365',
    3W:	'WIN_365',
    4Q	'MARKET_GROUP_PAIR_ID'
    AB	'FINANCIALS_PRICE_1'
    AC	'STATS_COLUMN'
    AD	'ADDITIONAL_DATA, TEAM_TOUCHDOWN_QUOTE'
    AE	STATS_CELL
    AF	ARCHIVE_FIXTURE_INFO
    AH	ASIAN_HOVER, FINANCIALS_MARKET_ODDS_1
    AI	ANIMATION_ID
    AJ	FINANCIALS_MARKET_ODDS_2
    AM	ANIMATION_ICON
    AO	ANIMATION_TOPIC
    AP	STATS_PANE
    AQ	FINANCIALS_CLOSE_TIME
    AS	ADDITIONAL_STATS, ANIMATION_SOUND, TEAM_FIELDGOAL_QUOTE
    AT	ANIMATION_TEXT, STATS_TAB
    AU	AUDIO_AVAILABLE
    AV	ARCHIVE_VIDEO_AVAILABLE
    BB	BUTTON_BAR
    BC	BOOK_CLOSES, CLOSE_BETS_COUNT
    BD	PULL_BET_DATA
    BE	BET
    BH	BLURB_HEADER
    BI	BUTTON_BAR_INDEX, BUTTON_SPLIT_INDEX
    BL	BASE_LINE
    BO	BASE_ODDS, OPEN_BETS_COUNT
    BS	BANNER_STYLE
    BT	INFO_POD_DETAIL_2
    C1	C1_ID, MINI_DIARY_C1
    C2	C2_ID, MINI_DIARY_C2
    C3	MINI_DIARY_C3
    CB	CLOSE_BETS_DISABLED, EXCLUDED_COUNTRIES, FINANCIALS_MARKET_NAME, CLOSE_BETS_ENABLED
    CC	BET_TYPE_PULL, COMPETITION_CODE
    CD	COMPETITION_DROPDOWN, FINANCIALS_TRADE
    CF	CONFIG
    CG	GLOBAL_CONFIG
    CI	CLASS_ID, MINI_DIARY, CUP_ICON
    CK	COMPETITION_KEY
    CL	CLASSIFICATION
    CM	BET_CALL_FEATURE_DISABLED, COMMENT
    CN	CHANNEL, COLUMN_NUMBER
    CO	COLUMN
    CP	CLOSE_BETS_PRESENTATION_PULL_DISABLED, CURRENT_PROGRESS, CURRENT_PERIOD
    CR	CLASS_ORDER, CLOSE_BET_RETURNS
    CS	CLASSIFICATIONS
    CT	COMPETITION_NAME
    CU	CURRENT_INFO
    D1	DATA_1
    D2	DATA_2
    D3	DATA_3
    D4	DATA_4
    D5	DATA_5
    DA	DIARY_DAY
    DC	DISPLAY_CLOCK
    DD	DISPLAY_DATE
    DE	DESCRIPTION
    DM	IN_PLAY_LAUNCHER_DISPLAY_MODE
    DN	DIARY_NAME, DRAW_NUMBER
    DO	DEFAULT_OPEN
    DP	DECIMAL_PLACES
    DR	DIARY_REFRESH
    DS	DISPLAY_SCORE
    DX	DISABLE_COLUMN_DISTRIBUTION
    DY	DIARY
    EA	EVENT_TIME
    EC	ERROR_CODE, EXCLUDED_COUNTRY_CODES
    ED	EXTRA_DATA_2, TEAM_ODDS_A
    EE	ETOTE_LINK_DATA
    EI	EVENT_ID
    EL	EXTRA_STATS_AVAILABLE
    EM	EMPTY
    EP	EXTRA_PARTICIPANTS
    ER	ERROR_LOGGING
    ES	EMBEDDED_STREAMING, EXTRA_SCORES
    ET	END_TIME, EVENT_TYPE
    EV	EVENT
    EW	EACH_WAY
    EX	EXTRA_DATA_1, TEAM_ODDS_H
    FD	FORCE_DISPLAY
    FF	FILTERING
    FI	FIXTURE_PARENT_ID
    FK	FINANCIALS_FEED_1
    FL	FINANCIALS_PERIOD_1, APN_FLUC
    FM	FINANCIALS_MARKET_1A
    FN	FINANCIALS_MARKET_1B
    FO	FINANCIALS_FEED_2, FORM_PULL
    FP	FINANCIALS_PERIOD_2, FIXED_PLACE
    FQ	FINANCIALS_MARKET_2A
    FR	FINANCIALS_MARKET_2B
    FS	FIXTURE_STARTED
    FW	FIXED_WIN
    GC	LOTTO_GAME_CODE
    GM	LOTTO_GAME_MARKET
    GR	GROUP
    HA	HANDICAP
    HD	HANDICAP_FORMATTED
    HI	HEADER_IMAGE, BET_HISTORY
    HM	MARKET_BAR
    HO	DEFAULT_OPEN_HOMEPAGE
    HP	SHOW_ON_HOMEPAGE
    HS	HASH
    HT	POD_HEADER_TEXT
    HU	INFO_BANNER_SUBHEAD2
    HV	POD_BODY_TEXT_2
    HW	HORSE_WEIGHT
    HY	HORSE_AGE
    I2	ID2
    IA	AUDIO_ICON, DIARY_AUDIO_AVAILABLE
    IB	IBOX
    IC	ICON
    ID	ID
    IF	IN_PLAY
    IG	IMAGE_ID
    IM	IMAGE, INCLUDE_OVERVIEW_MARKET
    IN	INFO, INFO_POD_IMAGE_URL
    IO	ITEM_ORDER
    IP	IN_PLAY_AVAILABLE_FLAG, PARENT_ID
    IQ	INFO_POD_IMAGE1
    IR	INRUNNING_INFO
    IS	INFO_POD_IMAGE_PATH1
    IT	TOPIC_ID
    IU	INFO_POD_IMAGE2
    JN	JOCKEY_PULL
    JY	JOCKEY
    KC	KIT_COLORS
    KI	KIT_ID
    L1	BREADCRUMB_LEVEL_1
    LA	LABEL, INFO_POD_LINK_1_ID
    LB	INFO_POD_LINK_1_DISPLAY_TEXT
    LC	EVENT_COUNT, INFO_POD_LINK_1_C1_ID
    LD	INFO_POD_LINK_1_C1_ID_TABLE
    LE	INFO_POD_LINK_1_C2_ID
    LF	INFO_POD_LINK_1_C2_ID_TABLE
    LG	INFO_POD_LINK_2_ID, SOCCER_LEAGUE
    LH	INFO_POD_LINK_2_DISPLAY_TEXT
    LI	INFO_POD_LINK_2_C1_ID
    LJ	INFO_POD_LINK_2_C1_ID_TABLE
    LK	INFO_POD_LINK_2_C2_ID
    LL	INFO_POD_LINK_2_C2_ID_TABLE
    LM	POD_ENCODED_URL_1, LIVE_MARKETS
    LN	POD_ENCODED_URL_2
    LO	DEFAULT_OPEN_LEFT
    LP	LIVE_IN_PLAY, INFO_POD_LINK_1_C3_ID
    LQ	INFO_POD_LINK_1_C3_ID_TABLE
    LR	INFO_POD_LINK_1_C3_SECTION_ID, LAST_RACES
    LS	PREVIOUS_SET_SCORE, SELECTED
    MA	MARKET
    MB	BET_CALL_V2_DISABLED, MAX_BET
    MC	CUSTOMER_TO_CUSTOMER_CALLING_FEATURE_DISABLED, COMMENT_V4, MARKET_COUNT
    MD	MATCHLIVE_PERIOD
    ME	MULTI_EVENT
    MF	MATCH_FLAG
    MG	MARKET_GROUP
    ML	MATCH_LENGTH
    MM	MERGE_MARKET
    MO	SECONDARY_UK_EVENT
    MP	MATCH_POSTPONED
    MR	CUSTOMER_TO_REPRESENTATIVE_CALLING_FEATURE_DISABLED, MORE_MARKETS
    MS	MEDIA_ID
    MT	BET_CALL_V2_TWILIO_DISABLED, MARKET_TYPE
    MU	MULTILINE
    MW	LOTTO_MAX_WINNINGS
    MY	MARKET_STYLE
    N2	NAME2
    NA	NAME
    NC	CLOTH_NUMBER
    NG	NGENERA
    NH	NEXT_HEADER
    NM	NON_MATCH_BASED
    NR	NON_RUNNER
    NT	NEUTRAL_VENUE_TEXT
    NV	NEUTRAL_VENUE
    OB	BANKER_OPTION, OPEN_BETS_ENABLED
    OD	ODDS
    OH	ODDS_HISTORY
    OO	ODDS_OVERRIDE
    OP	OPEN_BETS_PRESENTATION_PULL_DISABLED, OPEN_BETS
    OR	ORDER
    OT	OTHERS_AVAILABLE
    PA	PARTICIPANT
    PB	PUSH_BALANCE_ENABLED
    PC	PAGE_DATA_1, PARTICIPANT_COUNT, PARTIAL_CASHOUT_AVAILABLE
    PD	PAGE_DATA, POD, INFO_POD_TYPE, PULL_DELAY
    PE	PARTICIPANTS_EXCEEDED, PERIOD
    PF	PUSH_FLAG
    PG	PENALTY_GOALS, MATCHLIVE_ADDITIONAL_INFO, PAGE_TYPE
    PH	PHONE_ONLY
    PI	PLAYING_INDICATOR, AUS_TOTE_COMBINATION
    PN	CLOTH_NUMBER_PULL
    PO	POD_STACK_ORDER, POINTS
    PP	POD_OPEN
    PR	PREFERENCE_ID, MARKET_GROUP_USER_PREFERENCE
    PS	POD_STACK, PARTICIPANT_STATUS
    PT	PRODUCT_TYPE, POD_TYPE
    PV	PREMIUM_VERSION
    PX	NO_OFFER
    PY	PARTICIPANT_STYLE
    RA	RANGE
    RC	RESULT_CODE
    RD	RACE_DETAILS
    RE	BET_RETURNS
    RG	REGION
    RI	R4_COMMENT
    RO	DEFAULT_OPEN_RIGHT, RACE_OFF
    RS	RUNNER_STATUS, REGULAR_SINGLE
    RT	RESULTS_TEXT
    S1	MATCHLIVE_STATS_1
    S2	MATCHLIVE_STATS_2
    S3	MATCHLIVE_STATS_3
    S4	MATCHLIVE_STATS_4
    S5	MATCHLIVE_STATS_5
    S6	MATCHLIVE_STATS_6
    S7	MATCHLIVE_STATS_7
    S8	MATCHLIVE_STATS_8
    SA	CHANGE_STAMP, SUSPEND_ARRAY
    SB	SCOREBOARD_TYPE
    SC	SCORE, SCORES_COLUMN
    SD	AUDIO_ID
    SE	SECONDARY_EVENT
    SF	SPOTLIGHT_FORM
    SG	STAT_GROUP
    SI	IMAGE_ID_PULL, SECTION_ID
    SL	SCORES_CELL
    SM	START_TIME
    SN	DRAW_NUMBER_PULL
    SP	STAT_PERIOD
    SS	SHORT_SCORE, SUSPENDED_SELECTION
    ST	INFO_POD_DETAIL_1, STAT, POD_BODY_TEXT_1, STAKE
    SU	SUCCESS, SUSPENDED
    SV	MATCHLIVE_AVAILABLE
    SY	STYLE
    SZ	STAT_LOCATION
    T1	C1_TABLE, MINI_DIARY_T1, TEXT_1
    T2	C2_TABLE, MINI_DIARY_T2, TEXT_2
    T3	MINI_DIARY_T3, TEXT_3
    T4	TEXT_4
    T5	TEXT_5
    TA	TIME_ADDED
    TB	BREADCRUMB_TRAIL
    TC	BET_TOTE_TYPE, TEAM_COLOR
    TD	COUNTDOWN, TAX_DETAILS
    TE	TEAM
    TG	TEAM_GROUP
    TI	TMR_SERVER
    TL	LEAGUE_TOPIC, TOPIC_LIST
    TM	STAT_TIME, TMR_MINS
    TN	TRAINER_NAME
    TO	EMPTY_TOPIC_ID, PHONE_ONLY_LIST
    TP	TIME_STAMP
    TR	TAX_RATE, TOPIC_REFERENCE
    TS	TMR_SECS, TOTE_NAMES
    TT	TMR_TICKING
    TU	TMR_UPDATED
    TX	TAX_METHOD, TOPIC_LIST_EXCLUSIONS
    UC	CURRENT_INFO_V4
    UF	UPDATE_FREQUENCY
    VA	VALUE
    VC	MATCHLIVE_ANIMATION
    VD	VIRTUAL_DATA
    VI	VIDEO_AVAILABLE
    VL	VISIBLE
    VR	VIRTUAL_RACE
    VS	VIDEO_STREAM
    WG	WIZE_GUY
    WM	WINNING_MARGIN
    XB	CHECK_BOX
    XC	EXCLUDE_COLUMN_NUMBERS
    XI	EXTRA_INFO_NODE, TEAM_MATCHTOTAL_QUOTE
    XL	CONTROLLER
    XP	SHORT_POINTS
    XT	EXTRA_TIME_LENGTH
    XY	MATCHLIVE_COORDINATES
    ZA	TIMEZONE_ADJUSTMENT
    _V	PADDOCK_VIDEO_AVAILABLE
}
