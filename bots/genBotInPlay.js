const botEngine = require('./modules/botEngine.js');

async function init() {
    let bot = await botEngine.init('bet365inPlay', insertEvent);
    botEngine.startScraping(parsePages);

    async function insertEvent(event) {
        //bot.socket.emit('message', 'init event'+JSON.stringify(event));
        botEngine.emit('debug','inserting event '+JSON.stringify(event));
        try {
            let pg = await gotoLiveMatch(event, bot.jobs.pages[0]);
            bot.pagesToParse.push({p: pg, pc: bot.jobs.pages[0].parsingClasses, event:{h:event.h,g:event.g}});
            event.page.push(pg);
            bot.events.push(event);
        } catch (error) {
            bot.socket.emit('error-handling', {text:'error in init Matche', data:event, error:error});
        }
    }

    async function gotoLiveMatch(event, cmds) {
        bot.socket.emit('message', 'goto event '+JSON.stringify(event));
        botEngine.emit('debug',("navigating to event "+JSON.stringify(event)));
        let pg;
        pg = await botEngine.execCommand(cmds.np);
        for (let cmd of cmds.beforeParse) {
            await botEngine.execCommand(cmd, pg);
            await pg.waitFor(2000);
        }
        await click2GameExtraOdds(pg, event, cmds.parsingClasses);
        await pg.waitFor(2000);
        return pg
    }

    async function click2GameExtraOdds(pg, event, parsingClasses) {
        botEngine.emit('debug',("going to extra odds "+ JSON.stringify(event)));
        await pg.evaluate((data) => {
            let cnt = 0;
            for (let event of document.getElementsByClassName(data.pClasses.teams)) {
                if (event.innerText.indexOf(data.ev.h) >= 0 && event.innerText.indexOf(data.ev.g) >= 0) {
                    document.getElementsByClassName(data.pClasses.extraodds)[cnt].click();
                    break;
                }
                cnt++;
            }
        }, {ev: event, pClasses: parsingClasses});
    }

    async function parsePages(pageToPatse) {
        let pg = pageToPatse.p;
        let divClass = pageToPatse.pc;
        let ret;
        ret = await pg.evaluate((divClass) => {
            let domElements = document.getElementsByClassName(divClass.parsingClass);
            let mainobj = {tag: 'InPlay', markets: []};
            for (let el of domElements)
                mainobj.markets.push(translateObj(el));

            function translateObj(domEl) {
                for (let chld of domEl.childNodes)
                    if (chld.classList && chld.classList.contains(divClass.group) && !chld.classList.contains(divClass.groupOpen))
                        chld.click();
                for (let all of document.getElementsByClassName(divClass.allOddsButton)){
                    all.click();
                }
                return {data: domEl.innerText, market:domEl.getElementsByClassName('gl-MarketGroupButton_Text')[0].innerText, header:[]};
            }

            return mainobj;
        }, divClass);
        botEngine.emit('debug',("parse page "+ret.pg+" with tag "+ret.tag));
        return ret;
    }
}
init();