module.exports = function (type) {

    let mainBotConf = {
        headless: false,
        socketIP: '95.216.70.139',
        //socketIP: 'localhost',
        port: 3000,
        name: 'Vas Man Bot',
        maxPages: 2,
        availablePages: 2,
        type: 'bet365inPlay',
        mode: 'dev',
        interval: 2000,
        username: 'bot',
        password: '123'
    };

    switch (type) {
        case 'bet365inPlay' :
            mainBotConf.mode = 'debug';
            mainBotConf.username = 'inplay';
            mainBotConf.password = 'inplay';
            return mainBotConf;
            break;
        case 'bet365preGame':
            mainBotConf.type = 'bet365preGame';
            mainBotConf.username = 'pregame';
            mainBotConf.password = 'pregame';
            mainBotConf.interval = 4000;
            return mainBotConf;
            break;
        case 'eventManager':
            mainBotConf.type = 'eventManager';
            mainBotConf.username = 'manager';
            mainBotConf.password = 'manager';
            mainBotConf.interval = 4000;
            return mainBotConf;
            break;
    }

};
