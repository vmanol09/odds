const puppeteer = require('puppeteer');
let scrapInterval;
let bot = {};

async function initBot(botType, insertEvent) {
    let conf = require('../config/config.js')(botType);
    bot = {
        config: conf,
        browser: await puppeteer.launch({
            headless: conf.headless,
            defaultViewport: {
                width: 1920,
                height: 1080
            },
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox',
                '--disable-dev-shm-usage',
                '--disable-accelerated-2d-canvas',
                '--disable-gpu',
                '--window-size=1920x1080'
            ]
        }),
        jobs: [],
        events: [],
        pagesToParse: []
    };

    bot.socket = require('socket.io-client')('http://' + conf.socketIP + ':' + conf.port + '/bots');
    initSockets(bot, insertEvent);
    return bot;
}

function emit(mode, infoText, data) {
    if (bot.config.mode === mode) bot.socket.emit(mode, {text: infoText, data: data});
}

function formatData(data) {
    markets = {};
    for (let market in data)
        markets[data[market].market] = formatRow(data[market].data);

    return markets;

    function formatRow(data) {
        let ret = {};
        let strArray = data.split('\n');
        let strs = [], strfound = true, oddsCnt=0;
        strArray.splice(0,1);
        let doubleString = false, doubleValue = '';

        for (let i=0; i<strArray.length;i++) {
            if ((strArray[i].substr(0,1) === '+' || strArray[i].substr(0,1) === '-')) {
                doubleString = true;
                doubleValue = strArray[i];
                continue;
            }
            if (isNaN(Number(strArray[i])) || strArray[i].charCodeAt(0) === 160 ) {
                if (!strfound) strs = [];
                strs.push(strArray[i]);
                ret[strArray[i]] = [];
                oddsCnt = 0;
                strfound = true;
                continue;
            }
            strfound = false;
            if (doubleString) {
                doubleString = false;
                ret[strs[oddsCnt%strs.length]].push({val1:doubleValue, odd: strArray[i]});
            } else
                ret[strs[oddsCnt%strs.length]].push(strArray[i]);

            oddsCnt++;
        }
        return ret;
    }
}

async function startScraping(parseFunction, restartParse) {
    scrapInterval = setInterval(async () => {
            for (let i = bot.pagesToParse.length - 1; i >= 0; i--) {
                try {
                    let ret = await parseFunction(bot.pagesToParse[i]);
                    ret.markets = formatData(ret.markets);
                    ret.event = bot.pagesToParse[i].event;
                    bot.socket.emit('send-data', ret);
                    emit('debug', ("sending data from parsing page " + i + " from event " + (event.h + event.g) + " with dataLength " + ret.length));
                    bot.socket.emit('message', 'parsing page');
                } catch (error) {
                    bot.socket.emit('error-handling', {text: 'error parsing page', socket: bot.socket.id, error: error});
                    let deleted = bot.pagesToParse.splice(i, 1)[0];
                    if (restartParse) await restartParse(deleted);
                }
            }
        }, bot.config.interval);
}

function stopScraping() {
    emit('debug', 'stop scraping...');
    clearInterval(scrapInterval);
}

function initSockets(bot, insertEvent) {
    bot.socket.on('connect', ()=>{
        console.log('connected');
        console.log('Authenticating...');
        bot.socket.emit('authentication', bot.config);
    });
    bot.socket.on('unauthorized',(data)=>{
        console.log('unauthorized ', data)
    });
    bot.socket.on('authenticated', function (data) {
        console.log('Authenticated !!!');
        bot.socket.emit('set-identity', bot.config);
        bot.socket.emit('get-jobs', bot.config.type);

        bot.socket.on('disconnect', function (data) {
            console.log('disconnect', data);
        });

        bot.socket.on('set-name', function (data) {
            emit('debug', 'got event set-name with data:', data);
            myName = data;
        });
        bot.socket.on('set-event', function (data) {
            emit('debug', 'got event set-event with data:', data);
            insertEvent(data);
        });
        bot.socket.on('del-event', function (data) {
            emit('debug', 'got event del-event with data:', data);
            deleteEvent(data);
        });
        bot.socket.on('jobs', function (data) {
            emit('debug', 'got event jobs with data:', data);
            bot.jobs = data;
        });
        bot.socket.on('start-parsing', function (data) {
            emit('debug', 'got event start-parsing with data:', data);
            startScraping(bot.pagesToParse)
        });
        bot.socket.on('stop-parsing', function (data) {
            emit('debug', 'got event stop-parsing with data:', data);
            stopScraping()
        });
        bot.socket.on('set-mode', (data) => {
            emit('debug', 'got event set-mode with data:', data);
            bot.config.mode = data;
        })
    })
}

async function deleteEvent(event) {
    try {
        let myEvent = bot.events.find(ff => (ff.h === event.h && ff.g === event.g));
        let cnt = 0;
        for (let p of myEvent.page) {
            emit('debug', 'deleting page #' + cnt + ' from event : ' + myEvent.h + ' V ' + myEvent.g);
            delete bot.pagesToParse.find(ff => ff.pg === p)[0];
            await p.close();
            cnt++;
        }
    } catch (e) {
        bot.socket.emit('error-handling', {text: e, explanationString: 'error on deleting event', ev: e});
    }
}

async function execCommand(cmd, pg) {
    bot.socket.emit('bot-exec', cmd);
    emit('debug', ('executing command ' + JSON.stringify(cmd)));
    try {
        switch (cmd.a) {
            case 'np' :
                let npg = await bot.browser.newPage();
                await npg.goto(cmd.loc);
                await npg.waitFor(8000);
                return npg;
                break;
            case 'gt' :
                await pg.goto(cmd.loc);
                await pg.waitFor(8000);
                break;
            case 'click' :
                await pg.click(cmd.loc);
                break;
        }
    } catch (error) {
        bot.socket.emit('error-handling', error);
        return;
    }
}

module.exports = {
    init: initBot,
    startScraping: startScraping,
    execCommand: execCommand,
    emit: emit
};