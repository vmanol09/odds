const botEngine = require('./modules/botEngine.js');

async function init() {
    let bot = await botEngine.init('bet365preGame', insertEvent);
    await botEngine.startScraping(parsePages, restartTab);

    async function clickGamesOdds(pg, event, parsingClasses) {
        //console.log('navigating to match ', event);
        await pg.evaluate((data) => {
            let cnt = 0;
            for (let event of document.getElementsByClassName(data.pClasses.teams)) {
                if (event.innerText.indexOf(data.ev.h) >= 0 && event.innerText.indexOf(data.ev.g) >= 0) {
                    document.getElementsByClassName(data.pClasses.oddsButton)[cnt].click();
                    break;
                }
                cnt++;
            }
        }, {ev: event, pClasses: parsingClasses});
    }

    async function parsePages(pageToParse) {
        let pg = pageToParse.p;
        let divClass = pageToParse.pc;
        var ret = await pg.evaluate((divClass) => {
            let domElements = document.getElementsByClassName(divClass.parsingClass);
            var mainobj = {tag: 'PreGame', markets: []};
            for (var el of domElements) {
                mainobj.markets.push(translateObj(el));
            }

            function translateObj(domEl) {
                for (var chld of domEl.childNodes) {
                    if (chld.classList && chld.classList.contains(divClass.group) && !chld.classList.contains(divClass.groupOpen)) {
                        chld.click();
                    }
                }
                for (var all of document.getElementsByClassName(divClass.allButtonExpand))
                    all.click();
                return { data: domEl.innerText };
            }
            return mainobj;
        }, divClass);
        return ret;
    }

    async function gotoTodayMatches(event, cmds) {
       // bot.socket.emit('message', 'goto event ' + event.h);
        var pg = await botEngine.execCommand(cmds.np);
        for (let cmd of cmds.beforeParse) {
            await botEngine.execCommand(cmd, pg);
            await pg.waitFor(2000);
        }
        await clickGamesOdds(pg, event, cmds.parsingClasses);
        await pg.waitFor(2000);
        return pg
    }

    async function insertEvent(event) {
        //console.log('event ', event);
        try{
            let pg = await gotoTodayMatches(event, bot.jobs.pages[0]);
            bot.pagesToParse.push({p: pg, pc: bot.jobs.pages[0].parsingClasses});
            bot.jobs.eventsToParse.push(event);
            event.page.push(pg);
            //TODO : check for matches next tabs
            let i = bot.jobs.eventsToParse.length - 1;
            //console.log('before count nav but ', i);
            let navBtns = await countNavButtons(pg, bot.jobs.pages[0].parsingClasses.navBarClass);
            for (var j = 1; j < navBtns; j++) {
                //console.log('making pages for game', i, ' tab ', j);
                let navPage = await gotoTodayMatches({h:event.h,g:event.g}, bot.jobs.pages[0]);
                event.page.push(navPage);
                let tabName = await gotoNextNavTab(navPage, j);
                await navPage.waitFor(4000);
                bot.pagesToParse.push({p: navPage, pc: bot.jobs.pages[0].parsingClasses, tabName:tabName ,event:{h:event.h,g:event.g}, tabIndex:j});
            }
        }catch(error) {
            bot.socket.emit('error-handling', {text:'error in init Event', data:event, error:error});
        }
    }

    async function restartTab(data) {
        let pg = await gotoTodayMatches(data.event, bot.jobs.pages[0]);
        bot.jobs.eventsToParse.push(data.event);
        //event.page.push(pg);
        let tabName = await gotoNextNavTab(pg, data.tabIndex);
        await pg.waitFor(4000);
        bot.pagesToParse.push({p: pg, pc: bot.jobs.pages[0].parsingClasses, tabName:tabName ,event:{h:data.event.h,g:data.event.g}, tabIndex:data.tabIndex});
    }

    async function countNavButtons(page, navclass) {
        return await page.evaluate((navclass) => {
            return document.getElementsByClassName(navclass).length;
        }, navclass);
    }

    async function gotoNextNavTab(page, num) {
        //console.log('going to market group ', num);
        bot.socket.emit('bot-exec', 'going to market group ' + num);
        return await page.evaluate((num) => {
            let ret =document.getElementsByClassName('cl-MarketGroupNavBarButton')[num].innerText;
            document.getElementsByClassName('cl-MarketGroupNavBarButton')[num].click();
            return  ret;
        }, num);
    }
}
init();